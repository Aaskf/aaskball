#!/usr/bin/python3

# This version of the code is managed by Aask ^_^

# Magic 8 Ball IRC bot
# Created by Lance Brignoni on 8.9.15
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from random import randint
import socket
import select
import random
import time
import creds
import channels
import datetime
import time
import base64
import os
import sys
import platform

# Import home-built code for fetching the dates of DEF CON expositions
import fetch_exp

operating_system = platform.platform()
# If we're running in linux, that means we're in prod. Check for lock file before starting. 
# If not, we don't need to load fcntl as this doesn't exist in Windows

if operating_system.lower().find("linux") != -1:
    import fcntl
    
    lf = open("/tmp/aaskball.lock",'w')

    try:
        fcntl.flock(lf, fcntl.LOCK_EX|fcntl.LOCK_NB)
    except:
        sys.exit('other instance already running')

    lf.write('%d\n'%os.getpid())
    lf.flush()

#server = "chat.freenode.net"
server = "162.213.39.42"

channel_array = channels.channel_array
botnick = creds.botnick
password = creds.password

#list of responses
responses = ["Not so sure", "42", "Most likely", "Who's asking?", "Chances are high", "Absolutely not", "Outlook is good", 
"I see good things happening", "Never", "Stop asking", "If lain wills it", "Yes but you must worship me", "No, sorry", "Yes!", "If you try hard", "Sorry, no",
"I see it happening in your lifetime", "Yes but be careful", "No, don't count on it", "It's a possibility", "No chance", "I'd imagine so",
"Negative", "Could be", "Unclear, ask again", "Yes", "No", "Possible, but not probable", "Eat 2 fortune cookies then ask again","IDK my bff Jill?",
"Remember to hug your true","Most importantly, under no circumstances should you [bzzzpt]","Make no attempt to solve it.","Weeeeeeeeeeeeeeeeeeeeee",
"You are kidding me.","Leave. It. alone.","Think about it: If that thing is important, why don't I know about it?","I know you don't believe this, but everything that has happened so far was for your benefit.",
"Hello?","Is anyone there?","Can you hear me?","Are you still listening?","DELTA DELTA DELTA", "git gud scrub ^_^","Not right now","COVFEFE-19 is probably what you're looking for"]

# Initialize list of DEF CONs
dc_events = fetch_exp.auto_exp_fetch()

#pings
def ping():
    ircsock.send(bytes("PONG :Pong\n", "UTF-8"))
#join channel
def joinchan(chan):
    ircsock.send(bytes("JOIN "+ chan +"\n", "UTF-8"))

def wait_for_line(socket, line):
    while(1):
        data = socket.recv(4096).split(bytes("\r\n", "UTF-8"))
        if len(data) == 0:
            #no data received, socket closed
            sys.exit(0)

        for curline in data:
            if line in curline.decode("UTF-8"):
                return line

#The two first lines are used to connect to the server through port 6667 which is the most used irc-port. 
#The third line sends the username, realname etc. 
#The fourth line assigns a nick to the bot and the fifth line identifies the user
#The last line then joins the configured channel.
ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ircsock.connect((server, 6667))

#do SASL
ircsock.send(bytes("CAP REQ :sasl\n", "UTF-8"))
wait_for_line(ircsock, "ACK :sasl")
ircsock.send(bytes("AUTHENTICATE PLAIN\n", "UTF-8"))
wait_for_line(ircsock, "AUTHENTICATE")
a = bytes("AUTHENTICATE ", "UTF-8")
a = a + base64.b64encode("%s\0%s\0%s".encode("UTF-8") % (creds.botnick.encode("UTF-8"), creds.botnick.encode("UTF-8"), creds.password.encode("UTF-8")))
a = a + bytes("\n", "UTF-8")
ircsock.send(a)
wait_for_line(ircsock, "SASL authentication successful")
ircsock.send(bytes("CAP END\n", "UTF-8"))

ircsock.send(bytes("USER "+ botnick +" "+ botnick +" "+ botnick +" :connected\n", "UTF-8"))
ircsock.send(bytes("NICK "+ botnick +"\n", "UTF-8"))
ircsock.send(bytes("NICKSERV :IDENTIFY %s\r\n" % password, "UTF-8"))
for channel in channel_array:
    joinchan(channel)
#ircsock.send(bytes("wenchbot "+" :"+ "starting up aaskball!" +"\n", "UTF-8"))
# Put our previous timestamp 2 mins ago so we don't have to wait for timers below. 
previous_timestamp = time.time() - 120
previous_user = botnick

#constantly checks for new messages
while 1:
    def answer_query():
        return(random.choice(responses))

    def sendmsg(chan , msg):
        ircsock.send(str.encode("PRIVMSG "+ chan +" :"+ msg +"\n", "UTF-8"))

    # Validate our websocket is up, otherwise kill our thread
    try:
        ready_to_read, ready_to_write, in_error = \
            select.select([ircsock,], [ircsock,], [], 5)
    except select.error:
        # connection error event here
        print('connection error')
        break

    ircmsg = ircsock.recv(2048) # receive data from the server
    
    if len(ircmsg) == 0:
        break

    ircmsg = ircmsg.strip(bytes('\n\r', "UTF-8")) # removing any unnecessary linebreaks.
    ircmsg_str = str(ircmsg)
    # We need to get the username from the message
    msg_user = ircmsg_str.split('!')[0].replace(':',"").replace('b',"").replace("'","")
    print ('Bot pulled this nick: ' + msg_user)
    msg_cmd = ircmsg_str.split(':')[-1].split("'")[0].replace("!","")
    print ('Bot pulled this command: ' + msg_cmd)

    # Now make the whole message lower for later processing
    #ircmsg = ircmsg.lower()

    print(ircmsg) # Here we print what's coming from the server

    # Set the correct channel to respond to
    channel=ircmsg_str.split(' PRIVMSG ')[-1].split(':')[0]

    #send message to channel if !ball is called
    if ircmsg.lower().find(bytes(":!aaskball-test", "UTF-8")) != -1: 
        sendmsg(channel, "Test successful! ^_^ ")
        time.sleep(1)

    elif ircmsg.lower().find(bytes(":!aaskball", "UTF-8")) != -1: 
        current_timestamp = time.time()
        spam_filter_timeout = current_timestamp - previous_timestamp
        if spam_filter_timeout >= 2:
            gender_reply = [bytes("gender", "UTF-8"),bytes("girl", "UTF-8"),bytes("boy", "UTF-8")]
            replied = 0
            for entry in gender_reply:
                if ircmsg.lower().find(entry) != -1 :
                    msg = "I'm binary. I don't mean I'm both, I mean I'm actually just ones and zeroes. Think of me as a bot."
                    sendmsg(channel, msg)
                    replied = 1
                    break    
            if replied != 1:
                sendmsg(channel, answer_query())
                

    elif ircmsg.lower().find(bytes(":!dc", "UTF-8")) != -1: 
            # This chunk of code will let users know about different con instances without spamming the channel :)
            if ircmsg.lower().find(bytes(":!dclist", "UTF-8")) != -1 :
                current_timestamp = time.time()
                spam_filter_timeout = current_timestamp - previous_timestamp
                if spam_filter_timeout <= 2:
                    send_to = str(msg_user)
                else:
                    send_to = channel

                sendmsg(send_to, "Here's a list of all of the DEF CON expositions I can tell you about:")
                # Create an array list from our EXPOSITIONS dict on the dc_events object
                exposition_list = list(dc_events.EXPOSITIONS.keys())

                # Add DC28 to the list
                exposition_list.insert(0,"cancelled")
                exposition_list.insert(1,"28")
                exposition_list = ", dc".join(exposition_list)
                sendmsg(send_to,exposition_list)
            else:
                dc_instance = msg_cmd.split("dc")[-1]
                # print ('Checking to see if we have recorded DC ' + dc_instance + "...")
                if dc_instance in dc_events.EXPOSITIONS :
                    temp = dc_events.EXPOSITIONS[dc_instance]
                    current_timestamp = time.time()
                    spam_filter_timeout = current_timestamp - previous_timestamp
                    if spam_filter_timeout <= 2:
                        send_to = str(msg_user)
                        #print("I want to send to: " + send_to)
                        sendmsg(send_to, "I already answered that in the channel!")
                        sendmsg(send_to, "Let's get that to you again without spamming everyone...") 
                    else:
                        send_to = channel 
                        # Update the previous run time on the channel
                        previous_timestamp = time.time()   

                    # Calculate how long since DEF CON **
                    today = datetime.date.today()
                    
                    dc_date= datetime.date(temp.YEAR, temp.MONTH, temp.DAY)
                    diff = today - dc_date
                    days_since_dc = str(diff.days)
                    msg = 'There have been ' + days_since_dc + ' days since ☠️DEF CON '+ str(temp.ITERATION) + '☠️ which was hosted '+ temp.MONTH_STR + ' ' + str(temp.DAY) +'-'+ temp.END_DAY +' '+ str(temp.YEAR)+""
                    sendmsg(send_to, msg)               
                    msg = 'That time around we were hosted at '+ temp.LOCATION
                    sendmsg(send_to, msg)
                
                elif ircmsg.lower().find(bytes(":!dc28", "UTF-8")) != -1: 
                    # This chunk of code will let users know how long until DEF CON 27 without spamming the channel :)
                    current_timestamp = time.time()
                    dc28_duration = current_timestamp - previous_timestamp
                    if dc28_duration <= 2:
                        send_to = str(msg_user)
                        #print("I want to send to: " + send_to)
                        sendmsg(send_to, "I already answered that in the channel!")
                        sendmsg(send_to, "Let's get that to you again without spamming everyone...") 
                    else:
                        send_to = channel 
                        # Update the previous run time on the channel
                        previous_timestamp = time.time()   
                    # Calculate how long until DEF CON 27
                    today = datetime.date.today()
                    dc28 = datetime.date(2020, 8, 6)
                    diff = dc28 - today
                    days_to_dc28 = str(diff.days)
                    msg = 'There are currently ' + days_to_dc28 + ' days until ☠️ DEF CON 28 ☠️ on August 6th - 9th 2020!'
                    sendmsg(send_to, msg)               
                    msg = 'This time around we are at the Caesar\'s Forum in Las Vegas, NV... SPACE F0RCE!!!'
                    sendmsg(send_to, msg)  
                else: 
                    # This chunk of code will let users know how long until DEF CON without spamming the channel :)
                    current_timestamp = time.time()
                    dc_duration = current_timestamp - previous_timestamp
                    if dc_duration <= 2:
                        send_to = str(msg_user)
                        #print("I want to send to: " + send_to)
                        sendmsg(send_to, "I already answered that in the channel!")
                        sendmsg(send_to, "Let's get that to you again without spamming everyone...") 
                    else:
                        send_to = channel 
                        # Update the previous run time on the channel
                        previous_timestamp = time.time()   
                    # Calculate how long until DEF CON 27
                    today = datetime.date.today()
                    dc = datetime.date(9999, 12, 31)
                    diff = dc - today
                    days_to_dc = str(diff.days)
                    msg = 'There are ' + days_to_dc + ' days until ☠️DEF CON '+ str(dc_instance) + '☠️ on December 31, 9999'
                    sendmsg(send_to, msg)               
                    msg = 'This time around we are at Milliway\'s, see you there Space Cadet!...SPACE F0RCE!!!'
                    sendmsg(send_to, msg)  

    elif ircmsg.lower().find(bytes(":!roll", "UTF-8")) != -1: 
        current_timestamp = time.time()
        roll_duration = current_timestamp - previous_timestamp
        
        if roll_duration <= 2:
            send_to = str(msg_user)
            #print("I want to send to: " + send_to)
            sendmsg(send_to, "Slow your roll buddy!")
        else:
            send_to = channel 
            # Update the previous run time on the channel
            previous_timestamp = time.time()   

        sides = ircmsg.decode("UTF-8").lower().split(" ")[-1]
        
        if not sides.isnumeric():
            number_of_sides = 42
        else:
            number_of_sides = int(sides)
        
        if number_of_sides <= 0 :
            dice_roll = 0
        else:
            dice_roll = randint(0,number_of_sides)
        
        msg = 'You rolled a 🎲 ' + str(dice_roll) + ' 🎲'
        sendmsg(send_to, msg)
    elif ircmsg.lower().find(bytes(":!cancelled", "UTF-8")) != -1: 
        # This chunk of code will let users know how long until DEF CON 27 without spamming the channel :)
        current_timestamp = time.time()
        cancelled_duration = current_timestamp - previous_timestamp
        if cancelled_duration <= 2:
            send_to = str(msg_user)
            #print("I want to send to: " + send_to)
            sendmsg(send_to, "I already answered that in the channel!")
            sendmsg(send_to, "Let's get that to you again without spamming everyone...") 
        else:
            send_to = channel 
            # Update the previous run time on the channel
            previous_timestamp = time.time()   
        # Calculate how long until DEF CON 27
        today = datetime.date.today()
        cancelled = datetime.date(2020, 5, 15)
        diff = cancelled - today
        days_to_cancelled = str(diff.days)
        msg = 'There are ' + days_to_cancelled + ' days until ☠️ DEF CON 28 ☠️ could be ACTUALLY cancelled due to ☕☢️ COVFEFE-19 ☢️☕'
        sendmsg(send_to, msg)               
        msg = 'WASH YOUR HANDS (YA FILTHY ANIMAL!), drink all the booze, and hack ALL the things'
        sendmsg(send_to, msg)

    elif ircmsg.lower().find(bytes("ping :", "UTF-8")) != -1: # respond to pings
        ping()

ircsock.shutdown(2)    # 0 = done receiving, 1 = done sending, 2 = both
ircsock.close()
