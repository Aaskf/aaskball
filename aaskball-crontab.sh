#!/bin/ash
#python3 ./aask-bot.py &

cd /volume1/homes/Aask/aaskball

function aaskbot_monitor {
   local current_pid=$( ps -ef | grep '[p]ython3 ./aask-bot.py' | awk '{print $2}' )
   
   # If we have a running process, we should check its age and restart it if it is too old
   # If it is not running, start it
   if [ $(echo $current_pid | grep "[0-9]") ];then
      # Fetch the current age of our PID
      local current_age=$( ps -ef | grep '[p]ython3 ./aask-bot.py' | awk '{print $5}' )# echo "Age on pid $current_pid is $current_age"
      
      # If we have letters in the current age, that means we rolled over to the next day, let's do a theraputic restart
      if [ $(echo $current_age | grep "[a-zA-Z]") ];then
         # Kill our current PID
         kill -9 $current_pid
         
         # Sleep just to be sure the previous pid ended properly
         sleep 5
         
         # Start a new pid
         python3 ./aask-bot.py &
      fi
   else
      # Initial start-up of script
      python3 ./aask-bot.py &
   fi
}

aaskbot_monitor
