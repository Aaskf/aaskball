# This is a file to fetch all the def con dates for my bot to serve

from urllib import request
from html.parser import HTMLParser
import time

class DEFCONExpedition:
    def __init__(self,INDEX,NAME,ITERATION,LOCATION,YEAR,MONTH,MONTH_STR,DAY,END_DAY):
        self.INDEX = 0 
        self.NAME = NAME
        self.ITERATION = ITERATION
        self.LOCATION = LOCATION
        self.YEAR = YEAR
        self.MONTH = MONTH
        self.MONTH_STR = MONTH_STR
        self.DAY = DAY
        self.END_DAY = END_DAY

class DCHTMLParser(HTMLParser):
    #def handle_starttag(self, tag, attrs):
        #print("Encountered a start tag:", tag)

    #def handle_endtag(self, tag):
        #print("Encountered an end tag :", tag)
        
    def __init__(self):
        self.EXP_COUNT = 0
        self.EXPOSITIONS = {}
        super().__init__()

    def handle_data(self, DATA):
        if(("DEF CON" in DATA) and ("was held" in DATA)):
            TEMP = DATA.replace("\\n\\t\\t\\t\\t\\t","")
            TEMP = TEMP.split("was")
            self.NAME = str(TEMP[0])
            NAME_SPLIT = self.NAME.split()
            ITERATION = ""
            # See if we can just sweep the number
            LENGTH = len(NAME_SPLIT)
            if(LENGTH == 3):
                ITERATION = NAME_SPLIT[-1]
            else:
                for i in range(2, LENGTH):
                    ITERATION = ITERATION + " " + NAME_SPLIT[i]

            self.COORDINATES = str(TEMP[1]).replace("  "," ").replace("- ","-").replace("\\'","'")

            self.DATE = self.COORDINATES.split(" held ")[1].split(" at ")[0]
            
            self.LOCATION = self.COORDINATES.split(" held ")[1].split(" at ")[1]

            STRING = "☠️  "+self.NAME+"☠️ was"+self.LOCATION+""
            STRING = STRING.replace("  "," ")
            self.ITERATION = ITERATION.replace(" ","")
            # print(STRING)
            # print("Edition:",self.ITERATION)
            self.YEAR = int(self.DATE.split(", ")[-1])
            self.MONTH = time.strptime(self.DATE.split(" ")[0].replace("'",""),"%B").tm_mon
            self.MONTH_STR = self.DATE.split(" ")[0].replace("'","")
            self.DAY = int(self.DATE.split(" ")[1].split("-")[0])
            self.END_DAY = str(self.DATE.split(" ")[1].replace(",","").split("-")[-1])
            self.EXPOSITIONS[self.ITERATION.lower()] = DEFCONExpedition(self.EXP_COUNT,self.NAME,self.ITERATION,self.LOCATION,self.YEAR,self.MONTH,self.MONTH_STR,self.DAY,self.END_DAY)
            self.EXP_COUNT += 1

def auto_exp_fetch():
    URL = "https://defcon.org/html/links/dc-archives.html"
    DATA = request.urlopen(URL).read()
    parser = DCHTMLParser()
    parser.feed(str(DATA))
    return parser

def test_exp_fetch():
    URL = "https://defcon.org/html/links/dc-archives.html"
    DATA = request.urlopen(URL).read()
    parser = DCHTMLParser()
    parser.feed(str(DATA))
    
    print("Please input a DEF CON expedition to search:")
    test = input().lower()

    if(test in parser.EXPOSITIONS):
        x = parser.EXPOSITIONS[test]
        print(str(x.NAME))
        print(str(x.ITERATION))
        print(str(x.LOCATION))
        print(str(x.DATE))
